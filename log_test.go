// Copyright 2014 Geoff Ford. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package log

import (
	"errors"
	"fmt"
	"io/ioutil"
	"strings"
	"testing"
)

type mockWriter struct {
	buf []byte
	err error
}

func (m *mockWriter) Write(p []byte) (n int, err error) {
	m.buf = make([]byte, 0, len(p))
	m.err = nil
	m.buf = append(m.buf, p...)
	return
}

func (m *mockWriter) Equals(msgType string, msg string) bool {

	var skipCnt = 4
	if len(msgType) == 4 {
		skipCnt = 5
	}

	var s string = strings.TrimSpace(string(m.buf))
	items := strings.Split(s, " ")
	if len(items) < skipCnt {
		m.err = errors.New(fmt.Sprintf("Split did not work as expected - %s", s))
		return false
	}
	if items[0] != strings.ToUpper(msgType) {
		m.err = errors.New(fmt.Sprintf("Expected %s, got %s", msgType, items[0]))
		return false
	}

	logMsg := strings.Join(items[skipCnt:], " ")
	if logMsg != msg {
		m.err = errors.New(fmt.Sprintf("Expected '%s', got '%s' from '%s'", msg, logMsg, s))
		return false
	}

	return true
}

func (m *mockWriter) checkLogEntry(t *testing.T, msgType string, format string) {
	if !m.Equals(msgType, fmt.Sprintf(format, msgType, world)) {
		t.Error(m.err.Error())
	}
}

var (
	dt            = "Debug"
	it            = "Info"
	wt            = "Warn"
	et            = "Error"
	pt            = "Panic"
	world         = "World"
	d, i, w, e, p = &mockWriter{}, &mockWriter{}, &mockWriter{}, &mockWriter{}, &mockWriter{}
	spfmt         = "%s %s"
	dashfmt       = "%s-%s"
)

func TestLog(t *testing.T) {

	logger := New(d, i, w, e, p)

	logger.Debug(dt, world)
	d.checkLogEntry(t, dt, spfmt)
	logger.Debugf(dashfmt, dt, world)
	d.checkLogEntry(t, dt, dashfmt)

	logger.Info(it, world)
	i.checkLogEntry(t, it, spfmt)
	logger.Infof(dashfmt, it, world)
	i.checkLogEntry(t, it, dashfmt)

	logger.Warn(wt, world)
	w.checkLogEntry(t, wt, spfmt)
	logger.Warnf(dashfmt, wt, world)
	w.checkLogEntry(t, wt, dashfmt)

	logger.Error(et, world)
	e.checkLogEntry(t, et, spfmt)
	logger.Errorf(dashfmt, et, world)
	e.checkLogEntry(t, et, dashfmt)
}

func TestNoFormatString(t *testing.T) {

	logger := New(d, i, w, e, p)

	logger.Debugf(empty, dt, world)
	d.checkLogEntry(t, dt, spfmt)

	logger.Infof(empty, it, world)
	i.checkLogEntry(t, it, spfmt)

	logger.Warnf(empty, wt, world)
	w.checkLogEntry(t, wt, spfmt)

	logger.Errorf(empty, et, world)
	e.checkLogEntry(t, et, spfmt)
}

func TestNoOutput(t *testing.T) {

	logger := New(ioutil.Discard, ioutil.Discard, ioutil.Discard, ioutil.Discard, ioutil.Discard)

	logger.Debug(dt, world)
	logger.Info(it, world)
	logger.Warn(wt, world)
	logger.Error(et, world)

	logger.Debugf(empty, dt, world)
	logger.Infof(empty, it, world)
	logger.Warnf(empty, wt, world)
	logger.Errorf(empty, et, world)
}
