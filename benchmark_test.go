package log

import (
	"io/ioutil"
	"testing"
)

func Benchmark1(b *testing.B) {

	b.StopTimer()

	logger := New(ioutil.Discard, ioutil.Discard, ioutil.Discard, ioutil.Discard, ioutil.Discard)

	str := "This is a test"

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		logger.Debug(str)
	}
}

type mockWriterB2 struct {
}

func (m *mockWriterB2) Write(p []byte) (n int, err error) {
	return
}

func (m *mockWriterB2) Equals(msgType string, msg string) bool {
	return true
}

func Benchmark2(b *testing.B) {

	b.StopTimer()

	logger := New(&mockWriterB2{}, ioutil.Discard, ioutil.Discard, ioutil.Discard, ioutil.Discard)

	str := "This is a test"

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		logger.Debug(str)
	}
}

func Benchmark3(b *testing.B) {

	b.StopTimer()

	logger := New(ioutil.Discard, ioutil.Discard, ioutil.Discard, ioutil.Discard, ioutil.Discard)

	thread := 1129
	str := "This is a test"

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		logger.Debugf("Thread(%v): %v", thread, str)
	}
}

func Benchmark4(b *testing.B) {

	b.StopTimer()

	logger := New(&mockWriterB2{}, ioutil.Discard, ioutil.Discard, ioutil.Discard, ioutil.Discard)

	thread := 1129
	str := "This is a test"

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		logger.Debugf("Thread(%v): %v", thread, str)
	}
}

func Benchmark5(b *testing.B) {

	b.StopTimer()

	logger := New(&mockWriterB2{}, ioutil.Discard, ioutil.Discard, ioutil.Discard, ioutil.Discard)
	logger.SetMicrosecondResolution()

	thread := 1129
	str := "This is a test"

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		logger.Debugf("Thread(%v): %v", thread, str)
	}
}
