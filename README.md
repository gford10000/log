# log - provides simplified management of logging based on LogTypes (Debug, Info, Warn, Error and Panic).

The motivation for this package is to allow easy and extensive logging by LogType throughout code, with the knowledge that unless the sink is provided (i.e. that it is
not ioutil.Discard) then the cost of the call is minimal (in my benchmarks it is 134ns without a sink, compared to 1350ns per call with a sink).

The LogTypes can share their sinks or write to independent sinks, whichever is preferred.

## Getting started

Current version of the library requires a latest stable Go release. If you don't have the Go compiler installed, read the official [Go install guide](http://golang.org/doc/install).

Use go tool to install the package in your packages tree:

```
go get bitbucket.com/gford10000/registryFuture
```

Then you can use it in import section of your Go programs:

```go
import "bitbucket.com/gford10000/log"
```

## Basic Example

```go
package main

import (
	"bitbucket.com/gford10000/log"
	"io/ioutil"
	"os"
)

func main() {

	logger := log.New(ioutil.Discard, ioutil.Discard, os.Stdout, os.Stderr, os.Stderr)

	logger.SetMicrosecondResolution()

	logger.Debug("This should not be reported")

	logger.Warn("This warning should be reported")

	for i := 0; i < 3; i++ {
	
		logger.Warnf("This is the %vth warning", i)
	}

	logger.Error("Error should be reported to StdErr")

}
```

