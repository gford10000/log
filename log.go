// Copyright 2014 Geoff Ford. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// log simplifies logging by providing both a formatting option and by explicitly defining sinks for each type of logging.
// Since creation of the log string (formatted or unformatted) is deferred until the sink is checked for existance, logging
// can be used liberally without a performance overhead.
package log

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
)

// LogType is a type alias identifying the type of logging available
type LogType int

// The available types of logging
const (
	Debug LogType = 1 << iota
	Info
	Warn
	Error
	Panic
)

// Logger provides simplified logging to each available LogType
type Logger interface {
	Debug(v ...interface{})
	Debugf(format string, v ...interface{})
	Info(v ...interface{})
	Infof(format string, v ...interface{})
	Warn(v ...interface{})
	Warnf(format string, v ...interface{})
	Error(v ...interface{})
	Errorf(format string, v ...interface{})
	Panic(v ...interface{})
	Panicf(format string, v ...interface{})
	SetMicrosecondResolution()
	Get(logType LogType) *log.Logger
}

// logger is the internal implementation of the Logger interface
type logger struct {
	debug *log.Logger
	info  *log.Logger
	warn  *log.Logger
	error *log.Logger
	panic *log.Logger
}

// New creates an instance of Logger with the specified sinks for each LogType.
// No logging will be performed for a given LogType if the supplied sink is ioutil.Discard
func New(debugSink, infoSink, warnSink, errorSink, panicSink io.Writer) Logger {

	l := new(logger)

	if debugSink != ioutil.Discard {
		l.debug = log.New(debugSink, "DEBUG ", log.Ldate|log.Ltime|log.Lshortfile)
	}
	if infoSink != ioutil.Discard {
		l.info = log.New(infoSink, "INFO  ", log.Ldate|log.Ltime|log.Lshortfile)
	}
	if warnSink != ioutil.Discard {
		l.warn = log.New(warnSink, "WARN  ", log.Ldate|log.Ltime|log.Lshortfile)
	}
	if errorSink != ioutil.Discard {
		l.error = log.New(errorSink, "ERROR ", log.Ldate|log.Ltime|log.Lshortfile)
	}
	if panicSink != ioutil.Discard {
		l.panic = log.New(panicSink, "PANIC ", log.Ldate|log.Ltime|log.Lshortfile)
	}

	return l
}

// SetMicrosecondResolution switches on the microsecond time resolution to logs
func (l *logger) SetMicrosecondResolution() {

	f := func(l *log.Logger) {
		if l != nil {
			l.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile)
		}
	}

	f(l.debug)
	f(l.info)
	f(l.warn)
	f(l.error)
	f(l.panic)
}

const (
	depth = 3
	empty = ""
)

// logMsg will join the supplied values together as a single line
func (l *logger) logMsg(writer *log.Logger, v ...interface{}) string {
	var s string = empty
	if writer != nil {
		s = fmt.Sprintln(v...)
		writer.Output(depth, s)
	}
	return s
}

// logMsgf will provide a formatted line based on the format string and values
func (l *logger) logMsgf(writer *log.Logger, format string, v ...interface{}) string {
	var s string = empty
	if writer != nil {
		if len(format) > 0 {
			if format[len(format)-1] != '\n' {
				format += "\n"
			}
			s = fmt.Sprintf(format, v...)
		} else {
			s = fmt.Sprintln(v...)
		}
		writer.Output(depth, s)
	}
	return s
}

// Debug will log to the Debug LogType
func (l *logger) Debug(v ...interface{}) {
	l.logMsg(l.debug, v...)
}

// Debug will log to the Debug LogType, formatted as requested
func (l *logger) Debugf(format string, v ...interface{}) {
	l.logMsgf(l.debug, format, v...)
}

// Warn will log to the Warn LogType
func (l *logger) Warn(v ...interface{}) {
	l.logMsg(l.warn, v...)
}

// Warn will log to the Warn LogType, formatted as requested
func (l *logger) Warnf(format string, v ...interface{}) {
	l.logMsgf(l.warn, format, v...)
}

// Info will log to the Info LogType
func (l *logger) Info(v ...interface{}) {
	l.logMsg(l.info, v...)
}

// Info will log to the Info LogType, formatted as requested
func (l *logger) Infof(format string, v ...interface{}) {
	l.logMsgf(l.info, format, v...)
}

// Error will log to the Error LogType
func (l *logger) Error(v ...interface{}) {
	l.logMsg(l.error, v...)
}

// Error will log to the Error LogType, formatted as requested
func (l *logger) Errorf(format string, v ...interface{}) {
	l.logMsgf(l.error, format, v...)
}

// Panic will log to the Panic LogType, and then panic
func (l *logger) Panic(v ...interface{}) {
	panic(l.logMsg(l.panic, v...))
}

// Panic will log to the Panic LogType in the requested format, and then panic
func (l *logger) Panicf(format string, v ...interface{}) {
	panic(l.logMsgf(l.panic, format, v...))
}

// Get provides access to the underlying log.Logger for the specified LogType
func (l *logger) Get(logType LogType) *log.Logger {

	if logType == Debug {
		return l.debug
	} else if logType == Info {
		return l.info
	} else if logType == Warn {
		return l.warn
	} else if logType == Error {
		return l.error
	} else if logType == Panic {
		return l.panic
	} else {
		panic("Unknown LogType value requested")
	}
}
